<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: feature recognition framework</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

  <!--
     Quick navigation script. Use a div with "toc-panel" class having a
     nested div with "toc" id to place the navigation panel.
    -->
    <script src="../js/jquery-3.5.1.min.js"></script>
    <script src="../js/toc.js"></script>
 </head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../features.html">features</a>/feature recognition
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='http://quaoar.su/blog/page/analysis-situs'>Download</a></td>
  <td class="header-menu"><a href="../features.html">Features</a></td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-featrec">Introduction to CAD feature recognition</h1>

<div class="toc-panel"><div id="toc"></div></div>

<h2 id="toc-featrec-motivation">Motivation</h2>

<p>
  <i>Form features</i> or simply <i>features</i> are groups of CAD faces or volume elements that are meaningful for a specific engineering activity or application. In most cases, <i>design</i> and <i>manufacturing</i> features are not the same things because a CAD part is not modelled (digitally) the same way it is manufactured. Regardless of its type, a feature is a carrier of domain knowledge associated with the geometry of a part. A cornerstone problem for bridging CAD and CAM software is how to match form features with the corresponding machining programs. We recommend our readers to familiarize themselves with the paper by <a href="../references.html#wu-1996">[Wu et al, 1996]</a> that gives a clear introduction to the feature recognition area of research.
</p>

<p>
  Although it might be possible to design a CAD part in terms of its ultimate manufacturing features, such an approach is not widely used and is generally considered a bad idea <a href="../references.html#vandenbrande-requicha-1993">[Vandenbrande and Requicha, 1993]</a>. Therefore, even when the design features are known from the originating CAD system, they have to be converted to the manufacturing features to generate NC code. Then, more often than not, the design features are not even available because of lossy data exchange between the employed CAD and CAM packages. As a result, feature recognition had become a fundamental problem in the computer-aided design field.
</p>

<p>
  There is a bunch of engineering workflows where feature recognition is highly demanded. Some of them are listed below:
</p>

<ol>
  <li class="text-block"><b>Manufacturability analysis.</b> Before passing a CAD part to a machining shop, one has to be sure that this part can be fabricated in the first place. The manufacturability analysis can be conducted to assess all holes, pockets, slots, fillets and chamfers on a CAD part for accessibility and fitting the manufacturing constraints.</li>

  <li class="text-block"><b>Direct editing.</b> Direct editing is opposed to history-based editing in the sense that direct editing operations (push/pull, face tweaking, etc.) do not rely on the availability of design features somewhere in a feature tree. Instead, direct editing operators perform local feature recognition in the affected area. Knowing local features allows for changing the part's geometry without corrupting its original design intent.</li>

  <li class="text-block"><b>CAD part simplification (defeaturing).</b> Strictly speaking, simplification (or "defeaturing") is yet another type of direct editing. Simplification is often used to prepare design models for numerical simulation or consumption by downstream systems that do not speak the "CAD language" natively (e.g., for gaming engines). To simplify a feature (e.g., to removal a drilled hole), one needs to recognize that feature in the CAD part in the first place.</li>

  <li class="text-block"><b>Manufacturing cost estimation.</b> Digital manufacturing (or "Manufacturing as a Service") is a growing industry. It is often necessary to assess the expected production costs for a CAD part before placing the order. Those costs would naturally include the milling, turning or 3D printing time that highly correlates with the part's features. Therefore, feature recognition is required for accurate machining time estimation.</li>

  <li class="text-block"><b>Manufacturing planning.</b> Even if the design features are known from the originating CAD system (and that is often not the case), automated CNC manufacturing would require specific <i>manufacturing features</i> instead of "form" as-designed features.</li>
</ol>

<p>
  To our knowledge, Analysis Situs is the first, and the only open-sourced CAD software aimed to provide general feature recognition functionality.
</p>

<h2 id="toc-featrec-naive">Why naive approach is not the best?</h2>

<p>
  By "naive" approach we mean the technique of iterating CAD model's faces and grouping them together to compose the features. Such scanning approach is not completely desperate, and, if implemented wisely, it can end up with a workable solution.
</p>

<div align="center"><img src="../imgs/situ_featrec-scanning.gif"/></div>

<p>
  At the same time, such a naive approach is fundamentally "blind" as here we do not employ any formalism to ground our recognition algorithm upon. What we do here is just visiting all the CAD faces and analyzing their analytical properties to deduce if they "look like a feature" or not. The following subsections uncover the principal complexities of the recognition process.
</p>

<!-- <h3 id="toc-featrec-naive-inter">Interacting or generalized features</h3> -->

<p>
  It is relatively easy to extract all plain cylindrical holes out of a CAD model if you are well equipped and know how to program (Python can be just fine, although we prefer C++ here). The things get more laborious if you need to distinguish between plain, countersunk, counterbored and counterdrilled holes.
</p>

<div align="center"><img src="../imgs/situ_featrec-drilled-holes.png"/></div>

<p>
  Looking at the image above, you can imagine a few more combinations of countersinks and counterbores, including variations of blind and through holes. The number of possible feature patterns is quite big even for such a simple thing as a drilled hole. Now imagine that these holes perforate each other:
</p>

<div align="center"><img src="../imgs/situ_featrec-interacting-holes.gif"/></div>

<p>
  Even without interactions, some holes could not be easily classified as they correspond to a sequence of milling operations, such as boring stacked up steps:
</p>

<div align="center"><img src="../imgs/situ_featrec-complex-machined-hole.png"/></div>

<p>
  In the presence of interacting and complex features, simple scanning technique becomes error-prone unless it is equipped with a rich set of topologic and geometric checks all applied following some specific recognition strategy. Elaborating such a strategy is not a trivial exercise, although the heuristics employed down the road might not be that complicated.
</p>

<!-- <h3 id="toc-featrec-naive-ds">Data structures</h3> -->

<p>
  The principal data structure guiding feature recognition is, of course, the boundary representation of a CAD part itself. Since Analysis Situs is based on OpenCascade, we employ its minimalistic B-rep structure that does not support neither user-defined attributes nor convenient methods of iteration (such as back-references and circulators). Therefore, a computationally efficient feature recognition algorithm should employ some extra data structures, such as:
</p>

<ol>
  <li class="text-block">Attributes holder. This data structure is aimed at storing all the geometric cues and "knowledge" about the CAD features as they are being extracted by the recognition algorithms progressively.</li>

  <li class="text-block">Adjacency graphs. These graph data structures allow us to focus on the topological relationships between the boundary elements (and possibly features) and formalize the connectivity of shapes without much care of their geometry.</li>

  <li class="text-block">Iterators. It is critical to have efficient iterators over the B-rep elements to be able to query, for example, all parent faces for the given edge.</li>
</ol>

<p>
  Having a rich B-rep modeller under the hood of a feature recognizer is essential not only because we need to represent a part's shape with all its boundaries. Besides that, we also need to ask various "geometric questions" in the course of recognition. Therefore, we have to leverage the modelling kernel's API to solve the recognition problem.
</p>

<!-- <h3 id="toc-featrec-naive-formalism">Lack of formalism</h3> -->

<p>
  A <i>sound idea</i> is the key ingerient to any geometric algorithm. If a good idea is missing (and that is often the case with naive scan approaches), the algorithm has good chances to become a "big ball of mud" <a href="../references.html#big-ball-of-mood">[Foote and Yoder, 1997]</a>, i.e. a piece of puzzled non-maintainable code.
</p>

<p>
  Many scientific papers and technical reports have been published with the aim to document a well-proven feature recognition method. One of such methods consists in injecting the formalism of graph theory aimed at building up the graph-based abstraction of the recognition problem. The driving idea here consists of converting the geometric representation of a CAD part to a pure topological representation which facilitates computation.
</p>

<h1 id="toc-graphbased">Graph-based feature recognition</h1>

<h2 id="toc-graphbased-intro">Introduction</h2>

<p>
  Analysis Situs is mainly focused on graph-based feature recognition. In this graph-based recognition method, the initial B-rep model is enriched with a supplementary descriptor named <a href="features_aag.html">Attributed Adjacency Graph (AAG)</a>. You can read more about the benefits of using graph formalism in our paper <a href="../references.html#slyadnev-graphs-2020">[Slyadnev et al, 2020]</a>.
</p>

<p>
  AAG was introduced as a concept back in eighties. You can read a paper by <a href="../references.html#joshi-chang-1988">[Joshi and Chang, 1988]</a> where they describe how an adjacency graph can help in the recognition of subtractive features, such as slots or steps.
</p>

<div align="center"><img src="../imgs/joshi-chang.png"/></div>

<p>
  AAG captures the neighborhood relation between the CAD faces. It does not matter how many edges the given two faces have in common: it the terms of adjacency, the graph nodes corresponding to those faces will have exactly one undirected arc in between. Then, there are attributes on the graph. The minimal information required for feature recognition is a Boolean flag indicating if the corresponding <a href="./features_check-dih-angles.html">dihedral angle</a> is concave or convex. The following image illustrates AAG of a box shape. We use a color code to denote the dihedral angle type (green for convex and red for concave).
</p>

<div align="center"><img src="../imgs/aag-box.png"/></div>

<p class="note">
  People apply different techniques for feature recognition. You're not obliged to use AAG. A more simple technique would be scanning your model like we mentioned before, and this way to use the same neighborhood relations between the B-rep entities without abstracting them to a graph structure. Iterating through B-rep faces, you can consult the types of surfaces, the corresponding normal fields, orientations, etc. Such techniques are based on programmatic rules (C++ logic) and do not take advantage of graph formalism in explicit form. The main reason of introducing AAG in Analysis Situs was to abstract topology from geometry and this way switch from geometry to the language of graphs. In the terms of graph theory, a feature is then a subgraph of an AAG.
</p>

<p>
  The idea of graph-based feature recognition is finding features as subgraphs of AAG. This is basically what Analysis Situs was originally designed for: adding feature recognition logic on top of OpenCascade kernel. The following tools of graph theory become available with an AAG at hand:
</p>

<ol>
  <li>Finding <a href="./features_find-isomorphous-faces.html">isomorphous</a> subgraphs.</li>
  <li>Analysis of connected components.</li>
  <li>Virtual (topology-only) defeaturing done barely of a graph without affecting the model.</li>
</ol>

<div align="center"><img src="../imgs/aag-box-fillets.png"/></div>

<p>
  The following image illustrates an application of AAG to the recognition of sheet metal parts. We can exploit the fact that removing all thickness faces in a part separates the graph onto two connected components corresponding to the upper and lower sides of a two-sided part.
</p>

<div align="center"><img src="../imgs/aag-sm.png"/></div>

<p>
  In the same way, if we keep only the thickness faces in the graph, we will see a specific circuit pattern in the corresponding subgraph. We can then check if each node of the graph has two incident arcs and the ring is closed. If so, we can conclude that we found a train of thickness faces.
</p>

<div align="center"><img src="../imgs/aag-sm-thickness.png"/></div>

<p>
  Each node of the AAG has a 1-based index corresponding to the index of a face in Analysis Situs. AAG is represented with an adjacency matrix.
</p>

<h2 id="toc-graphbased-aag-output">Feature recognition output</h2>

<p>
  If we managed to find a subgraph of AAG corresponding to a certain feature, we should be able to output it as a result. A feature is essentially a <a href="./features_recognition-anatomy-of-feature.html">set of indices</a> that can be easily serialized to any persistent storage, e.g., a file.
</p>

<div align="center"><img src="../imgs/featrec-result.png"/></div>

<p>
  Keep in mind that any modification of the input geometry invalidates the reported indices. However, if you do not modify your geometry, the feature indices are guaranteed to stay the same on loading/writing your CAD data from/to file.
</p>

<h2 id="toc-graphbased-aag-api">AAG API</h2>

<p>
  To construct AAG, use the ctor of <span class="code-inline">asiAlgo_AAG</span> class passing there the <span class="code-inline">TopoDS_Shape</span> data structure as an argument. At construction time, the AAG will index your topology to assign a unique 1-based index with each face. Other exploration maps can be requested by explicit invocations, such as of <span class="code-inline">asiAlgo_AAG::RequestMapOf*()</span>. At construction time, the graph is build and the dihedral angle attributes are associated with the graph arcs.
</p>

<div class="code">
  Handle(asiAlgo_AAG) aag = new asiAlgo_AAG(shape);
</div>

<p>
  AAG is constructed on a heap memory. As you can see, it is a handled class, i.e., it is manipulated by the OpenCascade handle. Once the AAG handle goes out of scope, it decrements the reference counter in the AAG object, and the graph is automatically destructed. This means that you're not supposed to call the destructor explicitly.
</p>

<p class="note">
  It should be noted that any geometric modification done on your CAD model invalidates its AAG. E.g., if you remove a feature from your part, you should forget about the existing AAG and construct a new one. Therefore, AAG is valid as long as your CAD geometry remains "frozen."
</p>

<p>
  In the terms of code architecture, a good practice would be to pass the same instance of AAG through all recognizers you happen to implement in your software: e.g., the recognizers of drilled holes, chamfers, fillets, etc., would all work on the same instance of the graph.
</p>

<p>
  To access the initial shape from AAG, use its <span class="code-inline">asiAlgo_AAG::GetMasterShape()</span> method. You can always dump the contents of your AAG to a text stream calling its <span class="code-inline">asiAlgo_AAG::DumpJSON()</span> method.
</p>

<pre class="code">
std::string filename = "C:/tmp/dump.json";
std::ofstream filestream(filename);
//
if ( !filestream.is_open() )
  return 1; // Error.
  
aag->DumpJSON(filestream);
filestream.close();
</pre>

<p>
  The AAG data structure stores a stack of adjacency matrices representing its states. This stack allows for temporary reducing the entire graph to subgraphs. For example, if you want to keep only specific nodes in the graph, you can use the push/pop API provided by AAG.
</p>

<div align="center"><img src="../imgs/aag-stack.png"/></div>

<p>
  The following code demonstrates how one can extract all connected components in a subgraph represented by <span class="code-inline">feature</span> variable. In the code below, the feature indices are selected arbitrarily. In reality, these indices could come from some other recognition algorithms or even be selected by the user interactively.
</p>

<pre class="code">
asiAlgo_Feature feature;
//
feature.Add(10);
feature.Add(20);
feature.Add(13);
// ...
  
aag->PushSubgraph(feature);
{
  std::vector&lt;asiAlgo_Feature&gt; ccomps;
  aag->GetConnectedComponents(ccomps);

  // Iterate the connected components.
  for ( const auto& ccomp : ccomps )
  {
    // Do something...
  }
}
aag->PopSubgraph();
</pre>

<p>
  The <span class="code-inline">asiAlgo_AAG::PopSubgraph()</span> invocation restores the previous state of the graph available on the stack. This push/pop pair of functions is pretty instrumental as it allows to capture only the faces of interest and then undo the modification.
</p>

<p>
  To iterate over AAG, you can use one of the available subclasses of <span class="code-inline">asiAlgo_AAGIterator</span>. The following code illustrates how to iterate all the faces in the graph. For each face, we also check its neighbors in the loop using the <span class="code-inline">asiAlgo_AAG::GetNeighbors()</span> function.
</p>

<pre class="code">
for ( asiAlgo_AAGRandomIterator it(aag); it.More(); it.Next() )
{
  const int              fid  = it.GetFaceId();
  const asiAlgo_Feature& nids = aag->GetNeighbors(fid);
  // ...
}
</pre>

<p>
  If you want to virtually suppress some faces in the AAG while keeping the incident arcs stitched, use <span class="code-inline">asiAlgo_AAG::Collapse()</span> function. This function is useful if, for example, you want to check the neighbor faces across blends and chamfers while ignoring the latter (see the image below).
</p>

<div align="center"><img src="../imgs/collapsed-blends.png"/></div>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    Copyright &copy; Analysis&nbsp;Situs 2015-present &nbsp; | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

</body>
</html>
