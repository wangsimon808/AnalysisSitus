Release notes:

-------------------------------------------------------------------------------
v1.2.0, 11 December 2022

Milestone "ver 1.2": https://gitlab.com/ssv/AnalysisSitus/-/milestones/1#tab-issues
-------------------------------------------------------------------------------
- Add integration with NetGen.
- Add Collpase() method for virtual collapsing of graph nodes.
- Add IGES import.
- Introduce 'dump-autoread' command for saving and autoloading script window contents.
- Make it possible to drag & drop filenames into the scripting window.
- Enable interactive selection of edges in the wireframe mode.
- Enable "Explore..." action for the Part Node.
- Get rid of optionals and avoid using C++17 for better compatibility with older envs.
- Add point size parameter for point clouds visualization.
- Add color parameter for point clouds.
- Read part color defined in a STEP file into the Color parameter of the active part.
- Fix incorrect colors of imported STEP file for a turned part.
- Make it possible to select all faces with "Ctrl+A" key combination.
- Add color parameter for curves in the imperative viewer.
- Turn on visibility of the part on 'set-as-part' command.
- Add a bunch of useful interfaces to the imperative viewer.
- Introduce visual presentation parameters for vectors in the imperative viewer.
- If a cylinder/cone is selected, allow measuring its angle (#124).
- Make app icon red for the debug builds.
- Introduce a tool for checking the validity of facets (#127).
- Add a bunch of fixes for SDK installation.
- Enable /runcommand syntax for batch.
- Fix minor bug with color persistence in IV topo shapes.
- Add Tcl command to save XDE to STEP.
- Introduce specific treatment for seams on rotational surfaces (#133).
- Let convert-to-bsurf command operate on the entire part.
- Add yet another inspection tool to analyze vertex vexity.
- Add "find vertex" GUI dialog and `select-vertices` Tcl command.
- Add unit tests for the active data framework.
- Add optional vertex adjacency relations to AAG.
- Add non-manifold dihedral angle type.
- Enable subdomains in thickness calculator.
- Add SVG export.
- Make topo killer allow reducing shape dimensionality.
- Read original units from STEP files (#154).
- Add distance field based on boundary crossing splitting criterion.
- Add UV bounds AAG attribute.
- Print OpenCascade version in the console on start.
- Introduce check-clearance Tcl command.
- Add "clear" toolbar button.
- Configure num of UV isolines in the Hatching Node.
- Fix isoparametric lines are rendered far from the face location (#160).
- Add point-solid classifier.
- Add visualization facilities for discrete model (#164).
- Fix 'save-step' command that does not pass colors through (#138).
- Fix crash on adding BREP model to empty project.
- Fix weird result of blend recognition (#166).
- Fix incorrect construction of grid (discrete case) (#165).
- Store vexity array in VBFs.
- Make recognition of conical EBFs an option.
- Add possibility to generate facets for the active part without using XDE assets (#139).
- Polish helping hints for Tcl commands.
- Add `dfbrowse` command for the active project.
- Get rid of metadata element nodes (#170).
- Active Data: reuse labels of dead Nodes to expand newly added Nodes on.
- Hide "check dihedral angle" if only one face is selected (#126).
- Make asiExe the default startup project in cmake (#153).
- Add control of accuracy over surface entities (#100).
- Make edge/face/vertex finders work on nested arrays of indices (#172).
- Fix lost colors after part transformation and saving to STEP (#179).
- Add "Del" shortcut as defeaturing hotkey (#144).
- Move "resolve coincident topology" setting to Analysis Situs Node (#92).
- Add a function to reset colors in an XDE document.
- Add possibility to check thickness by click on a face (#57).
- Expose persistent props of curvature combs for edges (#180).
- Introduce help command to return the hints for commands (#149).
- Add possibility to save individual STEP files from XDE parts.
- Introduce JSON viewer with collapsible sections (#173).
- Show face/edge ID on detection (mouse move) (#21).
- Redesign command list dialog (#72).
- Add RTCD::IntersectRayAABB for point-to-bbox checking.
- Add sparse-point-cloud Tcl command.
- UI: Active Script - sort list of completer (#187).
- Add inner points for planar faces in repatch.
- Add asiAlgo_Utils::Str::AmendedFilename().
- Shift selection is not synchronized between UV and 3D viewers (#175).
- Make it possible to remove nodes (#193).
- Add coloring of the mesh edges (#190).
- Add IV interface for local axes frames.
- Add simple recognition of linearly extruded surfaces; Add 'check-canonical' command.
- Add possibility to scale up and down the text in the JSON viewer (#185).
- Make push-pull work in the negative direction as well.
- Fix UpdateAssemblies() and add asm-xde-transform.
- Fix problem in FBX reader (#207).
- Restore 'save-as' and 'load' commands for native file format (cbf).
- Disable cell locators again (45DQW005A.stp, issue #211).
- Make feature comment visible in the parameter editor.
- Add a function to automatically reorient a part.
-------------------------------------------------------------------------------
v1.1.0, 13 September 2021
-------------------------------------------------------------------------------
 - Read here: http://quaoar.su/blog/page/meet-analysis-situs-11
-------------------------------------------------------------------------------
v1.0.0, 20 March 2021
-------------------------------------------------------------------------------
 - Read here: http://quaoar.su/blog/page/analysis-situs-100
-------------------------------------------------------------------------------
v0.4.0, 15 May 2020
-------------------------------------------------------------------------------
- Linux-ready.
- Enhancements in GUI, including object browser and parameter editor.
- Add new handy Tcl commands.
- Migrate to VTK 8.2.
- Bugfix.
-------------------------------------------------------------------------------
v0.3.2, 15 October 2019
-------------------------------------------------------------------------------
- STEP reader now imports colored faces.
- Add a set of handy Tcl commands.
- Add more functionality to the experimental reverse engineering workbench.
- Improvements in face finder UI.
- Add visualization of execution graphs (tree functions of Active Data).
- Add deviation checker for points vs mesh/brep.
- Allow PNG dumps as vtkImageData from presentation managers.
- Enable user-defined part transformation (translation, rotation) from parameter editor.
-------------------------------------------------------------------------------
v0.3.1, 31 May 2019
-------------------------------------------------------------------------------
- Add possibility to colorize faces.
- Colored faces can be exported to STEP format now.
- Add zoom in/out with Ctrl+Up/Down keys in Tcl console.
- More parameters are now available for configuring objects via parameter editor.
- Enrich the available set of Tcl commands.
- Add finder for internal locations in the part shape.
- Enable backface coloring for B-Rep and triangulations.
- Improve internal architecture for use as a general-purpose CAD platform.
- Add more commands for interactive reverse engineering.
- Bugfix.
-------------------------------------------------------------------------------
v0.3.0, 04 March 2019
-------------------------------------------------------------------------------
- Rearrange control panels to give more logical and powerful interface.
- Add parameter editor and data dictionary.
- Remove useless (after the latest improvements) toggle button in 3D viewer.
- Add 'save-step' Tcl command.
- Add 'build-triangulation-obb' Tcl command.
- Add 'move-triangulation' Tcl command.
- Add visualization and selection of triangulation nodes.
- Prototype surface deviation checker.
- Prototype commands for saving and loading binary project files.
- Refactor curvature plot to become a general-purpose 2D plot.
- Prototype reverse engineering asset of Tcl commands.
- Add interactive selection of points in point clouds.
- Refactor purification algorithm for point clouds.
- Add commands to build bilinear Coons patches.
- Bugfix.
-------------------------------------------------------------------------------
v0.2.7, 17 November 2018
-------------------------------------------------------------------------------
- Add new Tcl commands and improve some existing.
- Refactor and improve interactive picking mechanism to make
  it more flexible.
- Improve unit testing engine.
- Add batch processing.
- Add functions to read STL and PLY meshes.
- Add computation and visualization of mesh norms in mesh nodes
  and by elements.
- Add mesh-based topology-preserving offsets.
- Add basic facilities for interactive construstion of countours on meshes for
  further reverse engineering.
- Implement JSON serialization for AAG and its attributes.
- Improve 'rebuild-edge' algorithm to make it more robust by following
  knowledge-guided computation principle.
- Improve KEV and KEF Euler operators.
- Start working on documenation pages also available online
  at http://www.analysissitus.org.
- General ergonomics improvements.
- Bugfix.
-------------------------------------------------------------------------------
v0.2.6, 19 June 2018
-------------------------------------------------------------------------------
- Add facilities to serialize B-curves and B-surfaces to JSON.
- Add dialog for modification of B-curves and B-surfaces via JSON.
- Output more precise values in 'eval-curve'/'eval-surf' commands.
- Integrate HLR algorithm of OpenCascade as 'hlr' command.
- Integrate useful modeling commands (e.g., 'cut', 'trim-curve', some others).
- Make VTK source for curve visualization more robust and precise.
- Add asiTestEngine library to support in-house unit testing.
- Bugfix.
-------------------------------------------------------------------------------
v0.2.5, 30 May 2018
-------------------------------------------------------------------------------
- Ported to OpenCascade 7.3.0.
- Add analysis of geometric tolerances.
- Add inequality solver.
- Add many useful Tcl commands.
- Add gl2ps integration.
- Bugfix.
-------------------------------------------------------------------------------
v0.2.4, 26 March 2018
-------------------------------------------------------------------------------
- Add dialog to manage visibility of pipelines for presented objects.
- Add visualization of control net for B-surfaces.
- Improve ergonomics of secondary viewers.
- Enable loading BREP/STEP by double click on filename.
- Add many useful Tcl commands.
- Add visualization of curvature combs and plots for curves.
- Introduce functionality for edge reconstruction by local topology analysis.
- Enable visualization of sub-shape locations as colored arcs in topology graphs.
- Develop curve fairing method in asiAlgo_Utils.
- Bugfix everywhere.
- Improve ergonomics of Tcl console and Commands dialog.
-------------------------------------------------------------------------------
v0.2.3, 22 December 2017
-------------------------------------------------------------------------------
- Bugfix.
- Introduce visualization of modification history as graph.
-------------------------------------------------------------------------------
v0.2.2, 14 December 2017
-------------------------------------------------------------------------------
- Bugfix.
- Introduce tools for topological naming and history.
-------------------------------------------------------------------------------
v0.2.1, 06 December 2017
-------------------------------------------------------------------------------
- Improve ergonomics.
- Add more Tcl commands including 'undo' and 'redo'.
- Improve topology killer.
-------------------------------------------------------------------------------
v0.2.0, 12 November 2017
-------------------------------------------------------------------------------
- Change license to 3-clause BSD.
- Get rid of unnecessary dependencies.
- Improve scripting abilities and add more Tcl commands.
-------------------------------------------------------------------------------
v0.1.8, 20 September 2017
-------------------------------------------------------------------------------
- Fix visualization problem with false-positive invalid faces.
- Let user change background color via context menu in part viewer.
-------------------------------------------------------------------------------
v0.1.7, 19 September 2017
-------------------------------------------------------------------------------
- Improve visualization tools for meshes (SDK only).
- Extend API of imperative plotter (SDK only).
- Introduce button to add B-Reps to existing part.
- Introduce Tcl scripting.
- Introduce error-resistant visualization based on face validity analysis.
- Enhance topology graph visualization with information on orientations.
- Implement topologicall killer (kill-edge and kill-face commands).
- Bugfix and tiny improvements in ergonomics.
-------------------------------------------------------------------------------
v0.1.6, 20 July 2017
-------------------------------------------------------------------------------
- License under MIT license conditions.
- Add CMake build system.
- Set dark color scheme.
- Add controls for projection views (top, left, front, etc.).
- Add visualization facilities for OpenCascade's triangulation.
- Bugfix and tiny improvements.
-------------------------------------------------------------------------------
v0.1.5, 15 April 2017
-------------------------------------------------------------------------------
- Get rid of OpenCascade visualization modules. Everything is now based on
  in-house VTK integration code and conventional VTK interactive services.
- Add face inversion function (context menu in the viewer).
- Add visualization of face normals (context menu in the viewer).
- Add tessellation refinement controls (Ctrl+T hotkey).
- Add gap filling function (button at main controls panel).
- General bugfix and tiny improvements are also there.
-------------------------------------------------------------------------------
v0.1.4, 02 February 2017
-------------------------------------------------------------------------------
- Make AAG the driving container for all sub-shapes. AAG is now used to get
  the indices of faces and edges. This allows to cache all those indices only
  once (when the model is loaded) and reuse whenever necessary.
- Introduce many cosmetic changes: version in the title bar, different
  visualization properties, different highlighting/selection colors, etc.
- Add status bar, logger, and progress indication widgets (draft).
- Fix weird errors on edge picking.
- Add border trihedron concept at algorithmic level.
- Add VTK pipeline for visualization of normal vectors associated with point
  clouds.
- Add tolerance setting for smooth edge detection.
-------------------------------------------------------------------------------
v0.1.3, 19 December 2016
-------------------------------------------------------------------------------
- Add possibility to save selected faces and edges to BREP-file.
- Bugfix.
-------------------------------------------------------------------------------
v0.1.2, 16 December 2016
-------------------------------------------------------------------------------
- Add AAG and topology graphs with visualization.
- Add dihedral angle checker.
- Add non-manifold edges checker.
- Add tools to detach (break sharing info) and kill faces.
- Fix visualization bug with incorrect feeding of face selection pipeline.
-------------------------------------------------------------------------------
v0.1.1, 14 December 2016
-------------------------------------------------------------------------------
- Experimental version of adaptive center of rotation (there is a problem with
  discontinuity at rotation start). Center of rotation is changed by overriding
  the focal point of the camera and repositioning window center.
- Cosmetic changes in UI: reordering buttons.
- Porting to VTK 7.1.
===============================================================================
