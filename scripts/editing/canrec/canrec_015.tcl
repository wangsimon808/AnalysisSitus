source $env(ASI_TEST_SCRIPTS)/editing/canrec/__begin

# Set working variables.
set datafile cad/chamfers/chamfers_21.stp

# Reference numbers.
set ref_nbSurfBezier     0
set ref_nbSurfSpl        0
set ref_nbSurfConical    8
set ref_nbSurfCyl        77
set ref_nbSurfOffset     0
set ref_nbSurfSph        0
set ref_nbSurfLinExtr    20
set ref_nbSurfOfRevol    0
set ref_nbSurfToroidal   0
set ref_nbSurfPlane      123
set ref_nbCurveBezier    0
set ref_nbCurveSpline    50
set ref_nbCurveCircle    161
set ref_nbCurveEllipse   0
set ref_nbCurveHyperbola 0
set ref_nbCurveLine      417
set ref_nbCurveOffset    0
set ref_nbCurveParabola  0

__convert-canonical
