source $env(ASI_TEST_SCRIPTS)/editing/canrec/__begin

# Set working variables.
set datafile cad/canrec/rhino_solidworks_comparison_Sketched-Bend4_cr.stp

# Reference numbers.
set ref_nbSurfBezier     0
set ref_nbSurfSpl        0
set ref_nbSurfConical    0
set ref_nbSurfCyl        250
set ref_nbSurfOffset     0
set ref_nbSurfSph        0
set ref_nbSurfLinExtr    19
set ref_nbSurfOfRevol    0
set ref_nbSurfToroidal   0
set ref_nbSurfPlane      141
set ref_nbCurveBezier    0
set ref_nbCurveSpline    38
set ref_nbCurveCircle    500
set ref_nbCurveEllipse   0
set ref_nbCurveHyperbola 0
set ref_nbCurveLine      606
set ref_nbCurveOffset    0
set ref_nbCurveParabola  0

__convert-canonical
